with import <nixpkgs> {};

let
  _exemple_2 = import exemple_2/projet2/default.nix {};
  _exemple_3 = import exemple_3/default.nix {};
  _movie_db = import movie_db/default.nix {};
in

stdenv.mkDerivation {

  name = "pyconfr-2018-nix";

  buildInputs = [
  ];

  src = ./.;

  buildPhase = "";

  installPhase = ''
    mkdir -p $out
    cp -R slides_js $out/
    cp -R ${_exemple_2} $out/exemple_2
    cp -R ${_exemple_3} $out/exemple_3
    cp -R ${_movie_db} $out/movie_db
  '';
}

