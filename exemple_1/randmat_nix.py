#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python3Packages.numpy

import numpy as np

def print_randmat():
    print(np.random.random((2, 3)))

if __name__ == '__main__':
    print_randmat()
