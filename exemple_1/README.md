# Exemple 1 : script simple


## Sans Nix

```
virtualenv myvenv
source myenv/bin/activate
pip install numpy
./randmat.py
deactivate
```


## Avec Nix

### Exécution dans un nix-shell

```
nix-shell -p python3Packages.numpy --run ./randmat.py
```


### Script nix-shell

```
./randmat_nix.py
```


