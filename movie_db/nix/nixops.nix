{
  network.description = "movie_db";

  webserver = { config, pkgs, ... }: 
  let 
    _pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {};
    _movie_db = import ../default.nix { pkgs = _pkgs; };
  in
  { 
    networking.firewall.allowedTCPPorts = [ 5000 ];

    systemd.services.moviedb = { 
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "postgresql.service" ];
      serviceConfig = { 
        ExecStart = "${_movie_db}/bin/server.py";
      };
    };

    services.postgresql = {
      enable = true;
      initialScript = pkgs.writeText "backend-initScript" ''
        \cd ${_movie_db}/data
        \i movie_pg.sql
      '';
      authentication = "local all all trust";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}


