#include <Eigen/Dense>
#include <iostream>
#include <tuple>

using namespace Eigen;

std::tuple<int,int,VectorXi> myhist(Ref<const VectorXi> dates) {
    int d0 = dates.minCoeff();
    int d1 = dates.maxCoeff();
    VectorXi hist = VectorXi::Zero(d1 - d0 + 1);
    for (int k=0; k<dates.size(); k++) 
        hist[dates[k]-d0]++;
    return std::make_tuple<int,int>(std::move(d0), std::move(d1), hist);
}

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>

PYBIND11_MODULE(mymath, m) {
    m.def("myhist", &myhist);
}

