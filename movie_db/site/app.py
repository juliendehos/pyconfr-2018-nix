
from flask import Flask
from flask import render_template

import json
import movie_db
import mymath
import numpy

app = Flask(__name__)

@app.route('/')
def home_route():
    actors = movie_db.get_actors()
    return render_template('index.html', actors=actors)

@app.route('/<name>')
def hist_route(name):
    years = movie_db.get_years(name)
    hist = []
    d0 = 0
    d1 = 0
    if years:
        years_arr = numpy.asarray(years, dtype=int)
        d0, d1, hist_arr = mymath.myhist(years_arr)
        hist = hist_arr.tolist()
    dates = [d for d in range(d0, d1+1)]
    data_obj = { u"hist" : hist, u"dates" : dates }
    data_json = json.dumps(data_obj)
    return render_template('hist.html', name=name, data=data_json)

