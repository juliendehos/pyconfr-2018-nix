from setuptools import setup, Extension
import pkgconfig

exts = [Extension('mymath',
    ['cpp/mymath.cpp'],
    extra_compile_args=['-std=c++14', pkgconfig.cflags('eigen3')],
    )]

setup(
    name = 'movie_db',
    version = '0.1.0',
    ext_modules = exts,
    scripts = ['site/movie_db.py', 'site/server.py', 'site/app.py'],
    data_files = [
        ('bin/templates', 
            ['site/templates/index.html', 'site/templates/hist.html']),
        ('data', 
            ['data/movie_pg.sql', 'data/movie.csv']) ]
    )


