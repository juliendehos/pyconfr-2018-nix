CREATE DATABASE moviedb;
\connect moviedb

CREATE TABLE movie (movie_title TEXT, movie_year INTEGER, actor_name TEXT);
\copy movie FROM 'movie.csv' DELIMITER ';' CSV

CREATE ROLE movieuser WITH LOGIN PASSWORD 'toto';
GRANT SELECT ON TABLE movie TO movieuser;

