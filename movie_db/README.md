
# movie_db

## Déploiement avec nixops

- déploiement :

```
nixops create -d vm1 nix/nixops.nix
nixops deploy -d vm1 --force-reboot
firefox "http://192.168.56.101:5000" &
```

- monitoring :

```
nixops ssh -d vm1 webserver
systemctl status moviedb
...
exit
```


## Déploiement en local

- configuration de la base de données :

```
cd data
psql -U postgres -f movie_pg.sql
cd -
```

- exécution locale du serveur (flask) :

```
nix-shell --run "FLASK_APP=site/app.py flask run"
```

- exécution locale du serveur (wsgi) :

```
nix-shell --run "server.py"
```

- programme de test :

```
nix-shell --run "movie_db.py 'nicolas cage'"
nix-shell --run "movie_db.py"
```

