#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python3Packages.pandas

import pandas as pd

df_csv = pd.read_csv('movie_metadata.csv')

def register(movie_title, movie_year, actor_name):
    title = str(movie_title).strip().replace(';', '')
    actor = str(actor_name).strip().replace(';', '')
    if not (pd.isna(movie_year) or pd.isnull(title) or pd.isnull(actor_name)):
        year = int(movie_year)
        print("{};{};{}".format(title, year, actor))

for index, row in df_csv.iterrows():
    movie_title = row['movie_title']
    movie_year = row['title_year']
    register(movie_title, movie_year, row['actor_1_name'])
    register(movie_title, movie_year, row['actor_2_name'])
    register(movie_title, movie_year, row['actor_3_name'])

