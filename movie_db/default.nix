#{ pkgs ? import <nixpkgs> {} }:
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {} }:
with pkgs;

let 
  _pybind11 = callPackage ./nix/pybind11.nix { 
    inherit stdenv fetchFromGitHub cmake;
    python = python3;
  };
in 

python3Packages.buildPythonPackage {
  name = "movie_db";
  src = ./.;
  propagatedBuildInputs = [ 
    eigen
    _pybind11
    python3Packages.flask
    python3Packages.gevent
    python3Packages.numpy
    python3Packages.pkgconfig
    python3Packages.psycopg2
  ];
}

