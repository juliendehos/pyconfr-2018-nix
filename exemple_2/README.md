
# Exemple 2 : modules Setuptools


## Sans Nix

```
virtualenv myenv
source myenv/bin/activate
pip install ./projet1
pip install ./projet2
randmat2.py
deactivate
```


## Avec Nix

### Exécution dans un nix-shell

```
cd projet2
nix-shell --run randmat2.py
```

### Installation avec nix-env

```
cd projet2
nix-env -f . -i 
```

