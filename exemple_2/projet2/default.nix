{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {} }:

with pkgs;
python3Packages.buildPythonPackage {
  name = "projet2";
  src = ./.;
  propagatedBuildInputs = [
    (import ../projet1/default.nix { inherit pkgs; })
  ];
}

