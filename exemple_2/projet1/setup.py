from setuptools import setup

setup(name='projet1',
      version='0.1.0',
      packages=['projet1'],
      scripts=['projet1/randmat.py'],
      install_requires=['numpy'])
